package com.example.shoeapp.model;

public class ShoeModel {

    //declare variables
    String id;
    String shoeName;
    String shoeSize;
    String shoeBrand;
    String shoePhone;
    String shoeImage;

    //constructor
    public ShoeModel(){

    }


    //matching constructor
    public ShoeModel(String id,String shoeName,String shoeSize,String shoeBrand,String shoePhone,String shoeImage){
        this.id = id;
        this.shoeName = shoeName;
        this.shoeSize = shoeSize;
        this.shoeBrand = shoeBrand;
        this.shoePhone = shoePhone;
        this.shoeImage = shoeImage;
    }

    //generate


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShoeName() {
        return shoeName;
    }

    public void setShoeName(String shoeName) {
        this.shoeName = shoeName;
    }

    public String getShoeSize() {
        return shoeSize;
    }

    public void setShoeSize(String shoeSize) {
        this.shoeSize = shoeSize;
    }

    public String getShoeBrand() {
        return shoeBrand;
    }

    public void setShoeBrand(String shoeBrand) {
        this.shoeBrand = shoeBrand;
    }

    public String getshoePhone() {
        return shoePhone;
    }

    public void setshoePhone(String shoePhone) {
        this.shoePhone = shoePhone;
    }

    public String getShoeImage() {
        return shoeImage;
    }

    public void setShoeImage(String shoeImage) {
        this.shoeImage = shoeImage;
    }
}
