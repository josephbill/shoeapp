package com.example.shoeapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.shoeapp.views.SelectionActivity;

import java.util.ArrayList;

public class OnboardingScreen extends AppCompatActivity {

    private LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;
    private ViewPager onboard_pager;
    private OnboardingAdapter mAdapter;
    private Button btn_get_started;
    int previous_pos=0;
    ArrayList<OnboardingModel> onBoardItems=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding_screen);

        //view identification
        btn_get_started =  findViewById(R.id.btn_get_started);
        onboard_pager =  findViewById(R.id.pager_introduction);
        pager_indicator = findViewById(R.id.viewPagerCountDots);
        //method to add data to arraylist
        loadData();
        //new instance of the adapter class
        mAdapter = new OnboardingAdapter(this,onBoardItems);
        //viewpager methods
        onboard_pager.setAdapter(mAdapter);
        onboard_pager.setCurrentItem(0);
        onboard_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                // Change the current position intimation
                for (int i = 0; i < dotsCount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(OnboardingScreen.this, R.drawable.nonselected_item));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(OnboardingScreen.this, R.drawable.item_selected));


                int pos=position+1;

                if(pos==dotsCount&&previous_pos==(dotsCount-1))
                    show_animation();
                else if(pos==(dotsCount-1)&&previous_pos==dotsCount)
                    hide_animation();

                previous_pos=pos;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        btn_get_started.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OnboardingScreen.this, SelectionActivity.class);
                startActivity(intent);
            }
        });

         //call for setting of the dots in selected and non selected scenarios for our slides
        setUiPageViewController();

    }

    //method to add data to arrayList
    private void loadData(){
          //variables for collections
          int[] titles = {R.string.title1,R.string.title2,R.string.title3};
          int[] desc = {R.string.desc1,R.string.desc2,R.string.desc3};
          int[] images = {R.drawable.nike,R.drawable.heel,R.drawable.addidas};

          //loop for the slides
          for (int i = 0; i < images.length; i++){
              //new instance of the model class
              OnboardingModel onboardingModel = new OnboardingModel();
              onboardingModel.setImageId(images[i]);
              onboardingModel.setTitle(getResources().getString(titles[i]));
              onboardingModel.setDescription(getResources().getString(desc[i]));

              //add the items to arrayList
              onBoardItems.add(onboardingModel);
          }
    }

    //show animation
    public void show_animation(){
        //call to animation class to set up transistion
        Animation show = AnimationUtils.loadAnimation(this, R.anim.transitionup);
        //start animation on button
        btn_get_started.startAnimation(show);

        //
        show.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                btn_get_started.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                 btn_get_started.clearAnimation();
            }
        });

    }


    public void hide_animation()
    {
        Animation hide = AnimationUtils.loadAnimation(this, R.anim.transitiondown);

        btn_get_started.startAnimation(hide);

        hide.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {

                btn_get_started.clearAnimation();
                btn_get_started.setVisibility(View.GONE);

            }

        });


    }

    // setup the
    private void setUiPageViewController() {

        dotsCount = mAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(OnboardingScreen.this, R.drawable.nonselected_item));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(6, 0, 6, 0);

            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(OnboardingScreen.this, R.drawable.item_selected));
    }


}