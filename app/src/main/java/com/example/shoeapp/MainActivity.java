package com.example.shoeapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.shoeapp.views.SelectionActivity;

public class MainActivity extends AppCompatActivity {
    //duration of wait for screen
    private final int SPLASH_DISPLAY_LENGTH = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //handler class
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //intent
                Intent intent = new Intent(MainActivity.this, OnboardingScreen.class);
                startActivity(intent);
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}