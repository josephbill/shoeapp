package com.example.shoeapp.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.shoeapp.R;
import com.example.shoeapp.model.ShoeModel;
import com.example.shoeapp.views.ShoeDetails;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class ShoeAdapter extends RecyclerView.Adapter<ShoeAdapter.ShoeViewHolder> {

    Context context;
    List<ShoeModel> shoeModels;

    //matching constructor
    public ShoeAdapter(Context context , List<ShoeModel> shoeModels1){
        this.context = context;
        this.shoeModels = shoeModels1;
    }

    public static class ShoeViewHolder extends RecyclerView.ViewHolder{
        TextView shoeName,shoeBrand;
        Button btnShop;
        ImageView shoeImage,btnCall;
        CardView cardView;
        public ShoeViewHolder(@NonNull View itemView) {
            super(itemView);

            shoeBrand = itemView.findViewById(R.id.shoeBrand);
            shoeName = itemView.findViewById(R.id.shoeName);
            btnCall = itemView.findViewById(R.id.btnCall);
            btnShop = itemView.findViewById(R.id.btnShop);
            shoeImage = itemView.findViewById(R.id.shoe_image);
            cardView = itemView.findViewById(R.id.cardProduct);

        }
    }

    @NonNull
    @Override
    public ShoeAdapter.ShoeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.shoe_item, parent, false);
        ShoeAdapter.ShoeViewHolder shoeViewHolder = new ShoeAdapter.ShoeViewHolder(v); //new instance
        return shoeViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ShoeAdapter.ShoeViewHolder holder, int position) {
        //new ref to the model to get position of item
        ShoeModel shoeModel = shoeModels.get(position);

        //string variables
        String name_shoe = shoeModels.get(position).getShoeName();
        String name_size = shoeModels.get(position).getShoeSize();
        String name_phone = shoeModels.get(position).getshoePhone();
        String name_image = shoeModels.get(position).getShoeImage();
        String name_id = shoeModels.get(position).getId();
        String name_brand = shoeModels.get(position).getShoeBrand();

        //bind data to views
        holder.shoeName.setText(shoeModels.get(position).getShoeName());
        holder.shoeBrand.setText(shoeModels.get(position).getShoeBrand());
        holder.btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //call
                String phone = "tel:" + name_phone;
                context.startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(phone)));
            }
        });

        //bind the image
        Glide.with(context)
                .load(shoeModels.get(position).getShoeImage())
                .into(holder.shoeImage);

        //sharing data

        holder.btnShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ShoeDetails.class);
                intent.putExtra("image",shoeModels.get(position).getShoeImage());
                  context.startActivity(intent);
            }
        });

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateandDeleteDialog(name_id,name_image,name_phone,name_shoe,name_size,name_brand);
            }
        });


    }

    private void updateandDeleteDialog(String name_id, String name_image, String name_phone, String name_shoe, String name_size,String name_brand) {
        //raise a dialog box
        //raising a dialog
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.update_shoe, null);
        dialogBuilder.setView(dialogView);

        //view identification
        EditText editName = dialogView.findViewById(R.id.editName);
        EditText editPhone = dialogView.findViewById(R.id.editPhone);
        Button btnUpdate = dialogView.findViewById(R.id.btnUpdateShoe);
        Button btnDelete = dialogView.findViewById(R.id.btnDeleteShoe);

        //title to the alert
        dialogBuilder.setTitle("Update Shoe Product");
        //create and show
        final AlertDialog dialog = dialogBuilder.create();
        dialog.show();

        //onclick
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //capture input
                String newShoeName = editName.getText().toString().trim();
                String newShoePhone = editPhone.getText().toString().trim();

                if (TextUtils.isEmpty(newShoeName)){
                    Toast.makeText(context, "Shoe name cannot be empty", Toast.LENGTH_SHORT).show();
                } else {
                    updateShoe(name_id,name_image,newShoePhone,newShoeName,name_size,name_brand);
                    dialog.dismiss();
                }
            }
        });


        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteShoe(name_id);
                dialog.dismiss();
            }
        });



    }

    private boolean updateShoe(String name_id, String name_image, String newShoePhone, String newShoeName, String name_size,String name_brand) {
        //create ref to db
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Inventory").child(name_id);
        //create a new object of the model class
        ShoeModel shoeModel = new ShoeModel(name_id,newShoeName,name_size,name_brand
        ,newShoePhone,name_image);

        databaseReference.setValue(shoeModel);

        return true;
    }


    private boolean deleteShoe(String name_id) {
        //create ref to db
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Inventory").child(name_id);
        databaseReference.removeValue();

        return true;

    }


    @Override
    public int getItemCount() {
        return shoeModels.size();
    }
}
