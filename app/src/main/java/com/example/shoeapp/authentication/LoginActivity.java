package com.example.shoeapp.authentication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shoeapp.R;
import com.example.shoeapp.views.UploadActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.function.LongFunction;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class LoginActivity extends AppCompatActivity {
    Button btnCreate, btnLogin;
    TextView textPass;
    TextInputEditText loginEmail,loginPass;
    String email,pass;
    private FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        FirebaseApp.initializeApp(this);
        auth = FirebaseAuth.getInstance();

        btnCreate = findViewById(R.id.btnCreateAccount);
        btnLogin = findViewById(R.id.btnLogin);
        textPass = findViewById(R.id.forgotPass);
        loginEmail = findViewById(R.id.loginEmail);
        loginPass = findViewById(R.id.loginPassword);



        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,CreateAccountActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                         loginUser();
                        }
        });

        textPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,ForgotPassword.class);
                startActivity(intent);
            }
        });

    }
    //validations
    private void loginUser() {
        email = loginEmail.getText().toString().trim();
        pass = loginPass.getText().toString().trim();

        if (!TextUtils.isEmpty(email)){
            loginUserFirebase(email,pass);

        }  else {
            Toast.makeText(this, "Email and Password fields have an issue", Toast.LENGTH_SHORT).show();
            SweetAlertDialog errorDialog = new SweetAlertDialog(LoginActivity.this,SweetAlertDialog.ERROR_TYPE);
            errorDialog.setTitle("Email and Password Fields not correct");
            errorDialog.setCancelable(true);
            errorDialog.setCanceledOnTouchOutside(false);
            errorDialog.show();
        }


        if (!TextUtils.isEmpty(pass)){
            loginUserFirebase(email,pass);

        }  else {

            Toast.makeText(this, "Email and Password fields have an issue", Toast.LENGTH_SHORT).show();
            SweetAlertDialog errorDialog = new SweetAlertDialog(LoginActivity.this,SweetAlertDialog.ERROR_TYPE);
            errorDialog.setTitle("Email and Password Fields not correct");
            errorDialog.setCancelable(true);
            errorDialog.setCanceledOnTouchOutside(false);
            errorDialog.show();
        }

    }

    private void loginUserFirebase(String email, String pass) {
        //giving user progress
        SweetAlertDialog progressDialog = new SweetAlertDialog(LoginActivity.this,SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.setTitleText("Signing in.. Please wait");
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        auth.signInWithEmailAndPassword(email,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                  if (task.isSuccessful()){
                      progressDialog.dismiss();
                      SweetAlertDialog successDialog = new SweetAlertDialog(LoginActivity.this,SweetAlertDialog.SUCCESS_TYPE);
                      successDialog.setTitleText("Account Verified" + task.isSuccessful());
                      successDialog.setCancelable(true);
                      successDialog.show();
                      updateUi();
                  } else if (!task.isSuccessful()){
                      progressDialog.dismiss();
                      SweetAlertDialog errorDialog = new SweetAlertDialog(LoginActivity.this,SweetAlertDialog.ERROR_TYPE);
                      errorDialog.setTitleText("Error in signing in");
                      errorDialog.setCancelable(true);
                      errorDialog.setCanceledOnTouchOutside(false);
                      errorDialog.show();
                  }
            }
        });
    }

    private void updateUi() {
        Intent intent = new Intent(LoginActivity.this,UploadActivity.class);
        startActivity(intent);
    }
}