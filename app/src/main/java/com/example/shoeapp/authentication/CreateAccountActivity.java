package com.example.shoeapp.authentication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shoeapp.R;
import com.example.shoeapp.views.UploadActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CreateAccountActivity extends AppCompatActivity {
    TextView alreadyhave;
    Button btnCreate;
    TextInputEditText createEmail,createPass,createConPass;
    String email,password,conPass;
    //firebase
    private FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        //intialize firebase app
        FirebaseApp.initializeApp(this);
        //intialize the firebase auth service
        auth = FirebaseAuth.getInstance();

        alreadyhave = findViewById(R.id.alreadyHave);
        btnCreate = findViewById(R.id.btnCreateAccount);
        createEmail = findViewById(R.id.createEmail);
        createPass = findViewById(R.id.createPass);
        createConPass = findViewById(R.id.createConPass);

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               createAccount();
            }
        });
        alreadyhave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateAccountActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }


    //validation
    private void createAccount() {
        email = createEmail.getText().toString().trim();
        password = createPass.getText().toString().trim();
        conPass = createConPass.getText().toString().trim();
        //validation
        if (!TextUtils.isEmpty(email)) {
            //create account in firebase
            createAccountFirebase(email,password);
        } else {
            SweetAlertDialog errorDialog = new SweetAlertDialog(CreateAccountActivity.this,SweetAlertDialog.ERROR_TYPE);
            errorDialog.setTitle("Email and Password Fields not correct");
            errorDialog.setCancelable(true);
            errorDialog.setCanceledOnTouchOutside(false);
            errorDialog.show();

        }

        if (TextUtils.isEmpty(password)) {
            //create account in firebase
            createAccountFirebase(email,password);
        } else {

            SweetAlertDialog errorDialog = new SweetAlertDialog(CreateAccountActivity.this,SweetAlertDialog.ERROR_TYPE);
            errorDialog.setTitle("Email and Password Fields not correct");
            errorDialog.setCancelable(true);
            errorDialog.setCanceledOnTouchOutside(false);
            errorDialog.show();
        }

        if (password == conPass) {
            Toast.makeText(this, "Email and Password fields have an issue", Toast.LENGTH_SHORT).show();
            SweetAlertDialog errorDialog = new SweetAlertDialog(CreateAccountActivity.this,SweetAlertDialog.ERROR_TYPE);
            errorDialog.setTitle("Email and Password Fields not correct");
            errorDialog.setCancelable(true);
            errorDialog.setCanceledOnTouchOutside(false);
            errorDialog.show();


        } else {
            //create account in firebase
            createAccountFirebase(email,password);
        }
    }
    //creating account
    private void createAccountFirebase(String email, String password) {
        //giving user progress
        SweetAlertDialog progressDialog = new SweetAlertDialog(CreateAccountActivity.this,SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.setTitleText("Creating Account.. Please wait");
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        //method to create acc in firebase is : CreateAccountUsingEmailandPassword
        auth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
               if (task.isSuccessful()){
                   progressDialog.dismiss();
                   SweetAlertDialog successDialog = new SweetAlertDialog(CreateAccountActivity.this,SweetAlertDialog.SUCCESS_TYPE);
                   successDialog.setTitleText("User created successfully" + task.isSuccessful());
                   successDialog.setCancelable(true);
                   successDialog.show();
                   updateUi();
               } else if (!task.isSuccessful()){
                   progressDialog.dismiss();
                   SweetAlertDialog errorDialog = new SweetAlertDialog(CreateAccountActivity.this,SweetAlertDialog.ERROR_TYPE);
                   errorDialog.setTitleText("Error in creating account");
                   errorDialog.setCancelable(true);
                   errorDialog.setCanceledOnTouchOutside(false);
                   errorDialog.show();
               }
            }
        });

    }

    private void updateUi() {
        Intent intent = new Intent(CreateAccountActivity.this,LoginActivity.class);
        startActivity(intent);
    }
}