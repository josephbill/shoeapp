package com.example.shoeapp.views;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.shoeapp.R;
import com.example.shoeapp.model.ShoeModel;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.jaredrummler.materialspinner.MaterialSpinner;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class UploadActivity extends AppCompatActivity {
    MaterialSpinner materialSpinner;
    String selectedBrand,name_shoe,size_shoe,pay_shoe = "";
    TextView signOut;
    private FirebaseAuth auth;
    Button logOut,submit;
    TextInputEditText shoeName,shoeSize,shoePayment;
    Button shoeImage;
    ImageView prodImage;
    ProgressBar progressBar;

    //  //intialize request counter
        private static final int PICK_IMAGE_REQUEST = 1;
        //the path
        private Uri mImageUri;

        //firebase database and storage ref
        private DatabaseReference database;
        private StorageReference storage;
        private StorageTask uploadTask;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        FirebaseApp.initializeApp(this);
        auth = FirebaseAuth.getInstance();

        //intiatilizing firebase db and storage
        database = FirebaseDatabase.getInstance().getReference("Inventory");
        storage = FirebaseStorage.getInstance().getReference("InventoryImages");

        //view identification
        materialSpinner = findViewById(R.id.shoeBrands);
        logOut = findViewById(R.id.logOut);
        submit = findViewById(R.id.submit);
        shoeImage = findViewById(R.id.shoeImage);
        shoeName = findViewById(R.id.shoeName);
        shoeSize = findViewById(R.id.shoeSize);
        shoePayment = findViewById(R.id.shoePayment);
        prodImage = findViewById(R.id.prodImage);
        progressBar = findViewById(R.id.progress_bar);

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auth.signOut();
                finishAffinity();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (uploadTask != null && uploadTask.isInProgress()){
                    Toast.makeText(UploadActivity.this, "Upload still in progress", Toast.LENGTH_SHORT).show();
                } else {
                    submitInventory();

                }
            }
        });

        shoePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });

        //on item selected
        //set items
        materialSpinner.setItems("Select Shoe Brand","Nike","Addidas","Puma","New Balance","Alexander Mcqueen");

        //set listener to the material spinner
        materialSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                    selectedBrand = item.toString();
                Toast.makeText(UploadActivity.this, "Brand is " + selectedBrand, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void pickImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,PICK_IMAGE_REQUEST);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            //store the image picked to the URI path
            mImageUri = data.getData();

            //use glide to display image to user before upload
            Glide.with(this)
                    .load(mImageUri)
                    .into(prodImage);

        }
    }

   //gets the file extension
    private String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    private void submitInventory(){


//                  send data to firebase
            if (mImageUri != null){
                //taking image metadata to storage
                StorageReference fileReference = storage.child(System.currentTimeMillis()
                        + "." + getFileExtension(mImageUri));

                //using upload task to upload image
                uploadTask = fileReference.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        //delaying pushing data to realtime db until image is uploaded
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setProgress(0);
                            }
                        }, 500);
                        Toast.makeText(UploadActivity.this, "Upload is successful", Toast.LENGTH_SHORT).show();

                        //pick users input
                        name_shoe = shoeName.getText().toString().trim();
                        size_shoe = shoeSize.getText().toString().trim();
                        pay_shoe = shoePayment.getText().toString().trim();

                        //push content to realtime db
                        //task class to get the download url of the uploaded images
                        Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl(); //url to image from the storage
                        while(!urlTask.isSuccessful()); //checking whether i have my url //this line stops the upload process
                        Uri downloadURL = urlTask.getResult(); //this is what we submit to the model
                        //create an id for unique identification of records
                        String uploadID = database.push().getKey();
                        //pushing data to model class
                        ShoeModel shoeModel = new ShoeModel(uploadID,name_shoe,size_shoe,selectedBrand,pay_shoe,downloadURL.toString());
                        //execute the model to take data to firebase
                        database.child(uploadID).setValue(shoeModel);
                        SweetAlertDialog success = new SweetAlertDialog(UploadActivity.this,SweetAlertDialog.SUCCESS_TYPE);
                        success.setTitle("Upload Successful");
                        success.setCancelable(true);
                        success.setCanceledOnTouchOutside(false);
                        success.show();
                        //clear text
                        shoeName.setText("");
                        shoePayment.setText("");
                        shoeImage.setText("");
                        shoeSize.setText("");

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(UploadActivity.this, "Error from firebase " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {
                        double progress = (100.0 * snapshot.getBytesTransferred() / snapshot.getTotalByteCount());
                        progressBar.setProgress((int) progress);
                    }
                });

            } else {
                Toast.makeText(this, "No file selected", Toast.LENGTH_SHORT).show();
            }

        }
    }

