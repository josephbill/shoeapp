package com.example.shoeapp.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.shoeapp.R;
import com.example.shoeapp.authentication.LoginActivity;

public class SelectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);
    }

    public void vendor(View v){
        Intent intent = new Intent(SelectionActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    public void buyer(View v){
        Intent intent = new Intent(SelectionActivity.this,ProductsActivity.class);
        startActivity(intent);
    }
}