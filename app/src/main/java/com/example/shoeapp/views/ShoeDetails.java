package com.example.shoeapp.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.shoeapp.R;

public class ShoeDetails extends AppCompatActivity {
    ImageView imageView;
    String imagePath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shoe_details);

        imageView = findViewById(R.id.sharedImage);

        Intent intent = getIntent();
        imagePath = intent.getStringExtra("image");

        //load this to IV using glide
        Glide.with(this)
                .load(imagePath)
                .into(imageView);

    }
}