package com.example.shoeapp.views;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.example.shoeapp.R;
import com.example.shoeapp.adapter.ShoeAdapter;
import com.example.shoeapp.model.ShoeModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ProductsActivity extends AppCompatActivity {
   //declare widgets and classes
    private RecyclerView recyclerView;
    //adapter
    ShoeAdapter shoeAdapter;
    //model
    List<ShoeModel> shoeModelList;
    //database Ref
    private DatabaseReference databaseReference;
    //progress bar
    ProgressBar progressBar;
    //Query
    Query query;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        //instance of our database ref
        databaseReference = FirebaseDatabase.getInstance().getReference("Inventory");
        String prodcuct = "Addidas";
        query = databaseReference.orderByChild("shoeBrand").equalTo(prodcuct);
        //progress bar
        progressBar = findViewById(R.id.progress_circle);
        recyclerView = findViewById(R.id.recyclerProducts);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        progressBar.setVisibility(View.VISIBLE);

        shoeModelList = new ArrayList<>();
        shoeAdapter = new ShoeAdapter(ProductsActivity.this,shoeModelList);
        recyclerView.setAdapter(shoeAdapter);
        //reading data from firebase
        databaseReference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                //for loop to iterate the datasnapshot
                shoeModelList.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    //getting values and sending them to my model class

                    ShoeModel shoeModel = dataSnapshot.getValue(ShoeModel.class);
                    Log.d("values " , "values from firebase " + dataSnapshot);
                    //add new records fetched
                    shoeModelList.add(shoeModel);
                }

                shoeAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                SweetAlertDialog errorDialog = new SweetAlertDialog(ProductsActivity.this,SweetAlertDialog.ERROR_TYPE);
                errorDialog.setTitle("Something went wrong");
                errorDialog.setTitleText(String.valueOf(error));
                errorDialog.setCancelable(true);
                errorDialog.setCanceledOnTouchOutside(true);
                errorDialog.show();
                progressBar.setVisibility(View.GONE);
            }
        });



    }
}